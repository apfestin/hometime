# frozen_string_literal: true

class ReservationsController < ApplicationController
  before_action :set_reservation, only: %i[show update destroy]

  # GET /reservations
  def index
    @reservations = Reservation.all

    render json: @reservations
  end

  # POST /reservations
  def create
    parser = Parser.get_parser(params)
    if parser
      reservation_params = parser.parse_reservation(params)
      guest_params = parser.parse_guest(params)
      new_guest = Guest.find_or_initialize_by(email: guest_params[:email])

      if reservation = Reservation.find_by(code: reservation_params[:code])
        reservation.transaction do
          old_guest = reservation.guest
          new_guest.update!(guest_params)
          reservation.update!(guest: new_guest)
          old_guest.delete if old_guest.reservations.empty?
          reservation.update!(reservation_params)
        rescue ActiveRecord::RecordInvalid => e
          render json: { error: :invalid_format }, status: :unprocessable_entity
        end
      else
        ActiveRecord::Base.transaction do
          new_guest.update!(guest_params)
          reservation_params[:guest] = new_guest
          reservation = Reservation.create!(reservation_params)
        rescue ActiveRecord::RecordInvalid => e
          render json: { error: :invalid_format }, status: :unprocessable_entity
        end
      end

      render json: { id: reservation.id }, status: 200
    else
      render json: { error: :unknown_format }, status: :unprocessable_entity
    end
  end
end
