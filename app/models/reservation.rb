# frozen_string_literal: true

class Reservation < ApplicationRecord
  belongs_to :guest

  validates :start_date, comparison: { less_than_or_equal_to: :end_date }
  validates :code, presence: true
  validates :start_date, presence: true
  validates :end_date, presence: true

  validates :status, presence: true
  validates :currency, presence: true
  validates :payout_price_cents, presence: true
  validates :security_price_cents, presence: true
  validates :total_price_cents, presence: true

  def payout_price
    return unless payout_price_cents

    payout_price_cents / 100.0
  end

  def security_price
    return unless security_price_cents

    security_price_cents / 100.0
  end

  def total_price
    return unless total_price_cents

    total_price_cents / 100.0
  end

  def nights
    (end_date - start_date).to_i
  end
end
