# frozen_string_literal: true

module Parsers
  class PayloadOne < Parser
    STATUSES = %w[accepted initial].freeze

    def self.parse_reservation(data)
      {
        code: data[:reservation_code],
        start_date: data[:start_date],
        end_date: data[:end_date],
        total_guests: data[:guests],
        adults: data[:adults],
        children: data[:children],
        infants: data[:infants],
        status: data[:status],
        currency: data[:currency],
        payout_price_cents: get_price_cents(data[:payout_price]),
        security_price_cents: get_price_cents(data[:security_price]),
        total_price_cents: get_price_cents(data[:total_price])
      }
    end

    def self.parse_guest(data)
      guest_data = data[:guest] || {}
      {
        first_name: guest_data[:first_name],
        last_name: guest_data[:last_name],
        phone_numbers: [guest_data[:phone]],
        email: guest_data[:email]
      }
    end

    def self.verify_status(status)
      return nil unless STATUSES.include?(status)

      status
    end
  end
end
