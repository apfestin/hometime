# frozen_string_literal: true

module Parsers
  class PayloadTwo < Parser
    STATUSES = %w[accepted pending].freeze

    def self.parse_reservation(data)
      reservation_data = data['reservation'] || {}
      guest_data = reservation_data['guest_details'] || {}

      {
        code: reservation_data[:code],
        start_date: reservation_data[:start_date],
        end_date: reservation_data[:end_date],
        total_guests: reservation_data[:number_of_guests],
        adults: guest_data[:number_of_adults],
        children: guest_data[:number_of_children],
        infants: guest_data[:number_of_infants],
        status: verify_status(reservation_data[:status_type]),
        currency: reservation_data[:host_currency],
        payout_price_cents: get_price_cents(reservation_data[:expected_payout_amount]),
        security_price_cents: get_price_cents(reservation_data[:listing_security_price_accurate]),
        total_price_cents: get_price_cents(reservation_data[:total_paid_amount_accurate])
      }
    end

    def self.parse_guest(data)
      reservation_data = data['reservation'] || {}
      {
        first_name: reservation_data[:guest_first_name],
        last_name: reservation_data[:guest_last_name],
        phone_numbers: reservation_data[:guest_phone_numbers],
        email: reservation_data[:guest_email]
      }
    end

    def self.verify_status(status)
      return nil unless STATUSES.include?(status)

      status
    end
  end
end
