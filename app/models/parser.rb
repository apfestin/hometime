# frozen_string_literal: true

class Parser
  def self.parse_reservation(_data)
    raise 'Not implemented'
  end

  def self.parse_guest(_data)
    raise 'Not implemented'
  end

  def self.verify_status
    raise 'Not implemented'
  end

  def self.get_price_cents(price)
    return nil if price.nil?

    (price.to_f * 100).to_i
  end

  def self.parse_and_verify(payload)
    guest = Guest.new(parse_guest(payload))

    return false unless guest.valid?

    reservation_params = parse_reservation(payload)
    reservation_params[:guest] = guest
    reservation = Reservation.new(reservation_params)

    reservation.valid?
  end

  def self.get_parser(data)
    if Parsers::PayloadOne.parse_and_verify(data)
      Parsers::PayloadOne
    elsif Parsers::PayloadTwo.parse_and_verify(data)
      Parsers::PayloadTwo
    end
  end
end
