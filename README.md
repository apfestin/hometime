# Hometime

An API-only Rails application that provides an endpoint to allow partners to create and update reservations in Hometime. Different partners can have different formats of provided data so in this application, we use one parser for each format.

## I. Using the API
After you have set up your local machine with the instructions below, you can now access the endpoint:

```
POST localhost:3000/reservations
```
This endpoint allows both creating and updating reservations and guest information. So far, there are two formats that are being accepted and you might find some sample formats below in III.

As a return value, the API returns a `200 OK` and the `id` of the created/updated which the partner may use to take note of the reservation if they would like.

## II. Setting Up Your Local Machine
#### Local Database
```
bin/rails db:create
bin/rails db:migrate
```

#### Local Server
```
bin/rails s
````

## III. Test Data

### Payload Format #1

```
{
  "reservation_code": "YYY12345678",
  "start_date": "2021-04-14",
  "end_date": "2021-04-18",
  "nights": 4,
  "guests": 4,
  "adults": 2,
  "children": 2,
  "infants": 0,
  "status": "accepted",
  "guest": {
    "first_name": "Wayne",
    "last_name": "Woodbridge",
    "phone": "639123456789",
    "email": "wayne_woodbridge@bnb.com"
  },
  "currency": "AUD",
  "payout_price": "4200.00",
  "security_price": "500",
  "total_price": "4700.00"
}
```

### Payload Format #2

```
{
  "reservation": {
    "code": "XXX12345678",
    "start_date": "2021-03-12",
    "end_date": "2021-03-16",
    "expected_payout_amount": "3800.00",
    "guest_details": {
      "localized_description": "4 guests",
      "number_of_adults": 2,
      "number_of_children": 2,
      "number_of_infants": 0
      },
    "guest_email": "wayne_woodbridge@bnb.com",
    "guest_first_name": "Wayne",
    "guest_last_name": "Woodbridge",
    "guest_phone_numbers": [
      "639123456789",
      "639123456789"
    ],
    "listing_security_price_accurate": "500.00",
    "host_currency": "AUD",
    "nights": 4,
    "number_of_guests": 4,
    "status_type": "accepted",
    "total_paid_amount_accurate": "4300.00"
  }
}
```

