class CreateReservations < ActiveRecord::Migration[7.0]
  def change
    create_table :reservations do |t|
      t.string :code, index: { unique: true }
      t.date :start_date, null: false
      t.date :end_date, null: false
      t.integer :total_guests
      t.integer :adults
      t.integer :children
      t.integer :infants
      t.string :status
      t.references :guest

      t.string :currency
      t.integer :payout_price_cents
      t.integer :security_price_cents
      t.integer :total_price_cents
      t.timestamps
    end
  end
end

