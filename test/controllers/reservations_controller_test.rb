# frozen_string_literal: true

require 'test_helper'

class ReservationsControllerTest < ActionDispatch::IntegrationTest
  test 'should get index' do
    get reservations_url, as: :json
    assert_response :success
  end

  test 'should create reservation with payload one format' do
    params =
      YAML.load_file(File.open('test/fixtures/files/payload_one_data.yml')).with_indifferent_access

    assert_difference('Reservation.count') do
      post reservations_url, params:, as: :json
    end

    assert_response :success
  end

  test 'should update reservation' do
    params =
      YAML.load_file(File.open('test/fixtures/files/payload_two_data.yml')).with_indifferent_access

    assert_difference('Reservation.count') do
      post reservations_url, params:, as: :json
    end
    assert_response :success

    params[:reservation][:host_currency] = 'PHP'

    assert_no_difference('Reservation.count') do
      post reservations_url, params:, as: :json
    end
    assert_response :success

    reservation = Reservation.find_by(code: params[:reservation][:code])
    assert_equal(reservation.currency, 'PHP')
  end
end
