# frozen_string_literal: true

require 'test_helper'

class ParserTest < ActiveSupport::TestCase
  test 'it raises a Not implemented error for .parse_reservation' do
    exception = assert_raises Exception do
      Parser.parse_reservation({})
    end
    assert_equal('Not implemented', exception.message)
  end

  test 'it raises a Not implemented error for .parse_guest' do
    exception = assert_raises Exception do
      Parser.parse_guest({})
    end
    assert_equal('Not implemented', exception.message)
  end

  test 'it raises a Not implemented error for .verify_status' do
    exception = assert_raises Exception do
      Parser.verify_status
    end
    assert_equal('Not implemented', exception.message)
  end

  test '.get_price_cents coverts amounts to cents (x100)' do
    price_cents = Parser.get_price_cents(100.50)
    assert_equal(price_cents, 10_050)
  end

  test '.get_price_cents returns nil when amount is nil' do
    price_cents = Parser.get_price_cents(nil)
    assert_nil(price_cents)
  end

  test 'returns the correct parser given a data format' do
    payload_one_data =
      YAML.load_file(File.open('test/fixtures/files/payload_one_data.yml')).with_indifferent_access
    payload_two_data =
      YAML.load_file(File.open('test/fixtures/files/payload_two_data.yml')).with_indifferent_access

    assert_equal(Parser.get_parser(payload_one_data), Parsers::PayloadOne)
    assert_equal(Parser.get_parser(payload_two_data), Parsers::PayloadTwo)
  end

  test 'returns nil when no parser matches given data' do
    unknown_format_data =
      YAML.load_file(File.open('test/fixtures/files/unknown_payload_data.yml')).with_indifferent_access

    assert_nil(Parser.get_parser(unknown_format_data))
  end
end
