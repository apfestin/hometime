# frozen_string_literal: true

require 'test_helper'

class PayloadOneParserTest < ActiveSupport::TestCase
  test 'it parsers reservation data correcty based on format' do
    payload_data =
      YAML.load_file(File.open('test/fixtures/files/payload_one_data.yml')).with_indifferent_access

    parsed_data = Parsers::PayloadOne.parse_reservation(payload_data)

    assert_equal(parsed_data, {
                   code: 'YYY12345678',
                   start_date: '2021-04-14',
                   end_date: '2021-04-18',
                   total_guests: 4,
                   adults: 2,
                   children: 2,
                   infants: 0,
                   status: 'accepted',
                   currency: 'AUD',
                   payout_price_cents: 420_000,
                   security_price_cents: 500_00,
                   total_price_cents: 470_000
                 })
  end

  test 'it parsers guest data correcty based on format' do
    payload_data =
      YAML.load_file(File.open('test/fixtures/files/payload_one_data.yml')).with_indifferent_access

    parsed_data = Parsers::PayloadOne.parse_guest(payload_data)

    assert_equal(parsed_data, {
                   first_name: 'Wayne',
                   last_name: 'Woodbridge',
                   phone_numbers: ['639123456789'],
                   email: 'wayne_woodbridge@bnb.com'
                 })
  end

  test 'it verifies reservation status' do
    Parsers::PayloadOne::STATUSES.each do |status|
      assert_equal(Parsers::PayloadOne.verify_status(status), status)
    end

    assert_nil(Parsers::PayloadOne.verify_status('STATUS NOT IN STATUSES'))
  end
end
